// landing page heading section
const title = document.getElementById("title")
const descri = document.getElementById("description")
const url = document.getElementById("link")
const img = document.getElementById("image")

// indian news heading page
const title1 = document.getElementById("title1")
const descri1 = document.getElementById("description1")
const url1 = document.getElementById("linkIn")
const img1 = document.getElementById("images")

// landing page section of news
const leftCol = document.getElementById("leftCol")
const middleCol = document.getElementById('middleCol')
const rightCol = document.getElementById('rightCol')

// Indian news page section of news
const leftCol1 = document.getElementById("leftCol1")
const middleCol1 = document.getElementById('middleCol1')
const rightCol1 = document.getElementById('rightCol1')

// function for landing page heading news

async function mainheadFetch() {
  try {
     
    const response = await fetch(`https://newsapi.org/v2/top-headlines?country=us&apiKey=a33c0f15c73a4b209779385b16a9cbfd`);
    
    const data = await response.json();
    // get the api data
    const articles = data.articles;
      // append the content of heading news
      for (let i = 0; i < articles.length; i++) {
        const news = articles[i];
        title.textContent = news.title
        descri.textContent = news.description
        url.href = news.url
        img.src = news.urlToImage
    }
}
catch (error) {
  console.error('Error fetching data:', error);
}
}
// indian heading news
async function indianheadFetch() {
  try {
     
    const response = await fetch(`https://newsapi.org/v2/top-headlines?country=in&apiKey=a33c0f15c73a4b209779385b16a9cbfd`);  
    const data = await response.json();
     // get the data from api
    const articles = data.articles;
    //  append content to the heading 
      for (let i = 0; i < articles.length; i++) {
        const news = articles[i];
        title1.textContent = news.title
        descri1.textContent = news.description
        url1.href = news.url
        img1.src = news.urlToImage
    }
}
catch (error) {
  console.error('Error fetching data:', error);
}
}

// landing page left side news
async function leftFetch() {
    try {
       
      const response = await fetch(`https://newsapi.org/v2/everything?q=business&pageSize=8&apiKey=a33c0f15c73a4b209779385b16a9cbfd`);
      
      const data = await response.json();
      
      // console.log( data);
      const articles = data.articles;
      for (let i = 0; i < articles.length; i++) {
        const news = articles[i];
        // creating a div and append to the leftcol
        const leftDiv =document.createElement('div')
        leftCol.appendChild(leftDiv)
        // creating article tag and append to the div
        const leftarticle = document.createElement('article')
        leftDiv.appendChild(leftarticle)
        // creating a link tag for news url and append to article
        const leftLink = document.createElement ('a')
        leftLink.href = news.url
        leftarticle.appendChild (leftLink)
        // created img tag and append to link
        const leftImg = document.createElement ('img')
        leftImg.classList.add ('articleImage')
        leftImg.src = news.urlToImage
        leftLink.appendChild (leftImg)
        // created h3 tag for title and append to link
        const leftTitle = document.createElement('h3')
        leftTitle.textContent = news.title
        leftLink.appendChild(leftTitle)
      }
  // Process the data further or update UI here
      
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  }

  // middle news section
  async function middleFetch() {
    try {
      const response = await fetch(`https://newsapi.org/v2/everything?q=football&pageSize=5&apiKey=a33c0f15c73a4b209779385b16a9cbfd`)
    const data = await response.json();
    const articles = data.articles

    for (let i = 0; i < articles.length; i++) {
      const trendingNews = articles[i];

      // creating a div and append to the middlecol
      const middleDiv =document.createElement('div')
      middleCol.appendChild(middleDiv)
      // creating article tag and append to the div
      const middlearticle = document.createElement('article')
      middlearticle.classList.add('articleSec')
      middleDiv.appendChild(middlearticle)
      // creating a link tag for news url and append to article
      const middleLink = document.createElement ('a')
      middleLink.href = trendingNews.url
      middlearticle.appendChild (middleLink)
       // created img tag and append to link
      const middleImg = document.createElement ('img')
      middleImg.classList.add ('articleImage')
      middleImg.src = trendingNews.urlToImage
      middleLink.appendChild (middleImg)
      // created h3 tag for title and append to link
      const middleTitle = document.createElement('h3')
      middleTitle.classList.add('articleTitle')
      middleTitle.textContent = trendingNews.title
      middleLink.appendChild(middleTitle)
  }
    }
  catch (error) {
    console.error('Error fetching data:', error);
  }
}

// right news section
  async function rightFetch() {
    try {
      const response = await fetch(`https://newsapi.org/v2/everything?q=market&pageSize=8&apiKey=a33c0f15c73a4b209779385b16a9cbfd`);
      
      const data = await response.json();
      const articles = data.articles

      for (let i = 0; i < articles.length; i++) {
        const indianNews = articles[i];

        // creating a div and append to the middlecol
        const rightDiv =document.createElement('div')
        rightCol.appendChild(rightDiv)
        // creating article tag and append to the div
        const rightarticle = document.createElement('article')
        rightDiv.appendChild(rightarticle)
        // creating a link tag for news url and append to article
        const rightLink = document.createElement ('a')
        rightLink.href = indianNews.url
        rightarticle.appendChild (rightLink)
        // created img tag and append to link
        const rightImg = document.createElement ('img')
        rightImg.classList.add ('articleImage')
        rightImg.src = indianNews.urlToImage
        rightLink.appendChild (rightImg)
        // created h3 tag for title and append to link
        const rightTitle = document.createElement('h3')
        rightTitle.textContent = indianNews.title
        rightLink.appendChild(rightTitle)  
      }

    }
    catch (error) {
      console.error('Error fetching data:', error);

    }
  }
// indian page left side news
async function indianleftFetch() {
  try {
     
    const response = await fetch(`https://newsapi.org/v2/top-headlines?country=in&category=business&pageSize=8&apiKey=a33c0f15c73a4b209779385b16a9cbfd`);
    
    const data = await response.json();
    
    // console.log( data);
    const articles = data.articles;
    for (let i = 0; i < articles.length; i++) {
      const news = articles[i];
      // creating a div and append to the leftcol
      const leftDiv =document.createElement('div')
      leftCol1.appendChild(leftDiv)
      // creating article tag and append to the div
      const leftarticle = document.createElement('article')
      leftDiv.appendChild(leftarticle)
      // creating a link tag for news url and append to article
      const leftLink = document.createElement ('a')
      leftLink.href = news.url
      leftarticle.appendChild (leftLink)
      // created img tag and append to link
      const leftImg = document.createElement ('img')
      leftImg.classList.add ('articleImage')
      leftImg.src = news.urlToImage
      leftLink.appendChild (leftImg)
      // created h3 tag for title and append to link
      const leftTitle = document.createElement('h3')
      leftTitle.textContent = news.title
      leftLink.appendChild(leftTitle)
    }
// Process the data further or update UI here
    
  } catch (error) {
    console.error('Error fetching data:', error);
  }
}
// indian page middle side news
async function indianmiddleFetch() {
  try {
    const response = await fetch(`https://newsapi.org/v2/top-headlines?country=in&category=sports&pageSize=5&apiKey=a33c0f15c73a4b209779385b16a9cbfd`)
  const data = await response.json();
  const articles = data.articles
  console.log(articles);

  for (let i = 0; i < articles.length; i++) {
    const trendingNews = articles[i];

    // creating a div and append to the middlecol
    const middleDiv =document.createElement('div')
    middleCol1.appendChild(middleDiv)
    // creating article tag and append to the div
    const middlearticle = document.createElement('article')
    middlearticle.classList.add('articleSec')
    middleDiv.appendChild(middlearticle)
    // creating a link tag for news url and append to article
    const middleLink = document.createElement ('a')
    middleLink.href = trendingNews.url
    middlearticle.appendChild (middleLink)
     // created img tag and append to link
    const middleImg = document.createElement ('img')
    middleImg.classList.add ('articleImage')
    middleImg.src = trendingNews.urlToImage
    middleLink.appendChild (middleImg)
    // created h3 tag for title and append to link
    const middleTitle = document.createElement('h3')
    middleTitle.classList.add('articleTitle')
    middleTitle.textContent = trendingNews.title
    middleLink.appendChild(middleTitle)
}
  }
catch (error) {
  console.error('Error fetching data:', error);
}
}

// right news section
async function indianrightFetch() {
  try {
    const response = await fetch(`https://newsapi.org/v2/top-headlines?country=in&category=entertainment&pageSize=8&apiKey=a33c0f15c73a4b209779385b16a9cbfd`);
    
    const data = await response.json();
    const articles = data.articles
    console.log(articles);
    for (let i = 0; i < articles.length; i++) {
      const indianNews = articles[i];

      // creating a div and append to the middlecol
      const rightDiv =document.createElement('div')
      rightCol1.appendChild(rightDiv)
      // creating article tag and append to the div
      const rightarticle = document.createElement('article')
      rightDiv.appendChild(rightarticle)
      // creating a link tag for news url and append to article
      const rightLink = document.createElement ('a')
      rightLink.href = indianNews.url
      rightarticle.appendChild (rightLink)
      // created img tag and append to link
      const rightImg = document.createElement ('img')
      rightImg.classList.add ('articleImage')
      rightImg.src = indianNews.urlToImage
      rightLink.appendChild (rightImg)
      // created h3 tag for title and append to link
      const rightTitle = document.createElement('h3')
      rightTitle.textContent = indianNews.title
      rightLink.appendChild(rightTitle)  
    }

  }
  catch (error) {
    console.error('Error fetching data:', error);

  }
}
  // Call the fetchData function
indianrightFetch();
indianmiddleFetch();
indianleftFetch();
indianheadFetch();
mainheadFetch();
leftFetch();
middleFetch();
rightFetch();
  